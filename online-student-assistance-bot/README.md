# MastodonRobot

## Project Description

Based on Mashtodon, this project implements a BOT, aiming to help teachers and provide help for the daily study and life of school students to improve their learning efficiency.


## Significance
1.	Now remote education is more and more important, the demand is more and more strong
2.	Using automated robots to answer frequently asked questions, saving campus management manpower costs
3.	Many existing chatbots need to input a lot of information for a single query, and the path is too long. Each time you ask a question again, you re-enter the query.

## Function Description
The robot's functions are mainly divided into the following five modules:
1.	School basic information query Can check the school for the designated major, grade start time, fees, school contact information
2.	Teaching information query Through the robot to query the designated major, grade corresponding class time, place, teacher contact information. Assignment requirements for this course, deadline, reference books; Examination information query, the robot can be used to query the specific subject of the examination time, place, teacher contact information, examination content guidance and reference links
3.	Tutor meeting appointment
4.	Collection of Classroom opinions You can grade the teacher teaching you and you can type/submit your opinion
5.	Problem Collection
If the above questions are not within the scope of the student's inquiry, the student can list the questions first. We will gather these questions in one place for the teacher to check and reply.
