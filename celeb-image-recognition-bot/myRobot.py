#!/usr/bin/python

import ananas
import sys
import pymysql
from PIL import Image
import os
import hashlib
import hmac
import urllib
import urllib.request
import time
import requests
import base64
import json
import locale
from bs4 import BeautifulSoup
import re
import pytesseract

class selfRobot(ananas.PineappleBot):
	
	@ananas.daily(hour=6, minute=15)
	def toot(self):
		global location
		location = "E2080"
		tm_hour, tm_min = self.get_time()
		msg = self.today(tm_hour, tm_min)
		self.mastodon.toot(msg)
		print('Tooted: %s' % msg)
		
	 @ananas.reply
	def respond_similarity(self, status, user):
		self.post(similarity)
	
	def conmysql():
		conn= pymysql.connect(
			host='localhost',
			port = 3306,
			user='root',
			passwd='123456',
			db ='sys',
			charset='utf8'
			)
		return conn
		
	def Recognition():
		userdict = {}
		APP_ID = '22698666'
		API_KEY = '59xwQrGDUnudGUCanUFbFGIK'
		SECRET_KEY = '1GWcH9OqpFb9DcD5AgeIvxI7qSmdwgTC'

		client = AipFace(APP_ID, API_KEY, SECRET_KEY)
		filePath = "test.jpg"
			
		with open(filePath,"rb") as f:  
			base64_data = base64.b64encode(f.read())
		image = str(base64_data, 'UTF-8')
		imageType = 'BASE64'
		successful = []
		options = {}
		result = client.detect(image, imageType, options)

		groupIdList = "superstar"
		try:
			a = client.search(image, imageType, groupIdList)
			b = a['result']
			c = b['user_list']
			d = c[0]
			
			print(b)
			print(d['user_id'])
			print( d['score'])

		except TypeError :
			
			pass
		
	def GetToken():
		request = urllib.request.Request(host)
		request.add_header('Content-Type', 'application/json; charset=UTF-8')
		response = urllib.parse.urlopen(request).encode("utf-8")
		content = response.read()
		if (content):
			js = json.loads(content)
			# return js['refresh_token']
			return js['access_token']
		return None

	def read_picture(file1,file2):
		fp = open(file1, 'rb')
		img1 = fp.read()
		fp.close()
		basedata1 = base64.b64encode(img1)
		image01 = str(basedata1,"utf-8")
		
		
		fp = open(file2, 'rb')
		img2 = fp.read()
		fp.close()
		basedata2 = base64.b64encode(img2)
		image02 = str(basedata2,"utf-8")
		
		params = json.dumps([
			{"image":image01,"image_type":'BASE64'},
			{"image":image02,"image_type":'BASE64'}])
		return params

	def facecompar(file1,file2):	
		url = "https://aip.baidubce.com/rest/2.0/face/v2/match"
		response=requests.get(host)  
		token=eval(response.text)['access_token']  	
		request_url = url + "?access_token=" + token
		headers = {
			"content-type":"application/json;charset=utf-8"}
		params=read_picture(file1,file2)
		request = urllib.request.urlopen(url=request_url, data=params, headers=headers)
		response = urllib.request.urlopen(request)
		content = response.read()
		if content:
			content = json.loads(content)
			similar=content['result'][0]['score']
			return similar
			
	def crawling(name):
		url="http://baike.baidu.com/search/word?word='%s'" % name
		response = requests.get(url)
		response.encoding = 'utf-8'
		text = response.text
		soup = BeautifulSoup(text, 'lxml')
		global information
		for infor in soup.find_all('meta')[3:4]:
			information = (infor.get('content'))
		return information

	def facecrawling(name):
		url="http://baike.baidu.com/search/word?word='%s'"  % name
		response = requests.get(url)
		response.encoding = 'utf-8'
		text = response.text
		soup = BeautifulSoup(text, 'lxml')
		imgurl = soup.img.get('src')
		try:
			response = requests.get('%s' % imgurl)
			open('./star/'+name+'.jpg', 'wb').write(response.content) 
		except:
			print("Done!")

	def stardb():
		starstr="IU TaylorSwift"
		starslist=starstr.split()
		for star in starslist:
			inform = crawling(star)
			facecrawling(star)
			try:
				conn = conmysql()
				cur = conn.cursor()
				image = star+'.jpg'
				sql="INSERT INTO sys.mastodon (imgid, imgname, imgdata) VALUES  (%s, %s, %s);"
				cur.execute(sql,(3,star,image))
				cur.close()
				conn.commit()
				conn.close()
			finally:
				print("connitue")
				
			time.sleep(1.5)

		
	if __name__=="__main__":
		starname="IU"
		conn = conmysql()
		cursor = conn.cursor()
		sql="select * from sys.mastodon where imgname='%s'" % starname
		cursor.execute(sql)
		result = cursor.fetchone()[2]
		
		fp = open("/Users/HippoBiscuits/Downloads/image.jpg",'wb')
		img01 = fp.write(result)
		fp.close()
		
		fp = open("/Users/HippoBiscuits/Downloads/timg.jpg",'rb')
		img1 = fp.read()
		fp.close()
		
		fp = open("/Users/HippoBiscuits/Downloads/timg2.jpg", 'rb')
		img2 = fp.read()
		fp.close()
		
		facecompar(img1, img2)
		
		cursor.close()
		conn.close()
			
