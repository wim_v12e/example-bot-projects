import sys
import pymysql
from PIL import Image
import os
 
path = "./"
 
#读取图片文件
fp = open("/Users/HippoBiscuits/Downloads/timg.jpeg", 'rb')
img = fp.read()
fp.close()
 
#建立一个MySQL连接
database = pymysql.connect(
		host='localhost',
		port = 3306,
		user='root',
		passwd='123456',
		db ='sys',
		charset='utf8'
		)
# 存入图片
# 创建游标
cursor = database.cursor()
#注意使用Binary()函数来指定存储的是二进制
sql = "INSERT INTO sys.mastodon (imgid, imgname, imgdata) VALUES  (%s, %s, %s);"
args = ('2', 'Taylor Swift', img)
cursor.execute(sql, args)
 
database.commit()
# 关闭游标
cursor.close()
# 关闭数据库连接
database.close()

print("Done! ")
