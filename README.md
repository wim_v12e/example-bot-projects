This project contains some examples of Python bots for the Fediverse. The first four use the Ananas framework.

- simplest-examlpe-bot
- celeb-image-recognition-bot
- weather-forecast-bot
- online-student-assistance-bot
- image-processing-bot
