# For more information about the ways to interact with a Mastodon server please read the Mastodon API documentation:
# https://mastodonpy.readthedocs.io/en/stable/index.html

import ananas

class ExampleBot(ananas.PineappleBot):
# Open a text file and read all lines into a list
    def start(self):
        with open('trivia.txt', 'r') as trivia_file:
           self.trivia = list(trivia_file)
        # Get the last 10 messages from the home timeline
        # Other timelines are local, public, list and hashtag
        msgs = self.mastodon.timeline_home(limit=10)
        for msg in msgs:    
            print(   str( msg['account']['username'] )
                    +'<'
                    + str( msg['account']['acct'] )
                    + '> '
                    +': '
                    + str( msg['content'] )
                    )
        

# Every hour, at hh:17, post a line from the trivia.txt file
    @ananas.hourly(minute=17)
    def post_trivia(self):
        self.mastodon.toot(random.choice(self.trivia))

# When a user mentions your bot, reply with a line of trivia
    @ananas.reply
    def respond_trivia(self, status, user):
        # post a reply of the form "@<user account>: <trivia message>"
        self.mastodon.toot("@{}: {}".format(user["acct"], random.choice(self.trivia)))

