# To use [Ananas](https://github.com/chr-1x/ananas) to make a bot:

- Install `ananas`: 
    pip install ananas
- Create a config file, mine is `exBot.cfg`, see the archive. It's a simple text file.
- Create a Python class which inherits from `ananas.PineappleBot`. I called it `ExampleBot` in my example, `exBot.py` in the archive.
- Define the `start()` method and, if needed, for example to schedule a post or to reply to a post,  some decorators as explained in the Ananas documentation (https://github.com/chr-1x/ananas)
- Run the `ananas` script: 
    ananas -i exBot.cfg
    - The script will ask for the email and password of your account on the Mastodon server
- You only have to do this once, after that you can run the script as: 
    ananas exBot.cfg

